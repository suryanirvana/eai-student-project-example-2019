# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> I Made Gede Surya Nirvana Prima Murthi - 1806173550
> 
>
>[Heroku Link](advprog-final-surya.herokuapp.com)

## Twelve Factors Issues
### Config
Some of the important-variables, that store sensitive information, is still exposed. For example text.analytics.api.key and server.port. To solve this problem, we need to change them to use environment variable ANALYTICS_API_KEY. Also, we need to set the environment variable in the GitLab and Heroku.

### Logging
Logs provide visibility into the behavior of a running app. In this problem, I found one in ```ScienceDirectPaperFinder```. It still uses ```System.err.println``` instead of Logs. Therefore, we need to change it to ```log.error```.

## Proof Task 2
![count](TASK2.png)