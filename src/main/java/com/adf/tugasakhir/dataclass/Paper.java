package com.adf.tugasakhir.dataclass;

import com.adf.tugasakhir.model.Conference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Conference model.
 */
@Entity
@Table(name = "paper")
@Data
public class Paper {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "title")
    private String title;

    @NotNull
    @Column(name = "abstrak")
    private String abstrak;

    @NotNull
    @Column(name = "url")
    private String url;

    @ManyToOne
    private Conference conference;

    public Paper(String title, String url, String abstrak) {
        this.title = title;
        this.abstrak = abstrak;
        this.url = url;
    }
}
