package com.adf.tugasakhir.service;

import com.adf.tugasakhir.dataclass.Documents;
import com.adf.tugasakhir.dataclass.KeyPhrasesResult;

/**
 * TextAnalysis 
 * This service will give you the appropriate text analyis result based on the
 * service that EAI have provided.
 */
public interface TextAnalysisService {

    /**
     * Will give you the key phrases inside of a document in form
     * of a KeyPhrasesResult object. 
     * @param documents
     * @return
     */
    public KeyPhrasesResult extractKeyPhrases(Documents documents);
}